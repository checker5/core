��    5      �  G   l      �     �     �     �     �     �     �               "     /     6     >     C     P     b     n  
   ~     �     �     �     �     �     �     �     �                    '     9     H     Q     ^     r     �     �     �  
   �     �     �     �     �     �     �               )     B     [     s     �     �  (   �  .   �  /     2   4  3   g  -   �  5   �  5   �     5	     L	     [	     s	     �	     �	     �	     �	     �	     �	     �	     �	     �	  
   
     
     -
      A
     b
  
   w
     �
  C   �
     �
     �
  �   �
     �     �     �     �  
   �  	   �     �          $     =     J     N     g  6   s  ,   �  2   �  ;   
  +   F  5   r  3   �  *   �     )                 	          5                              -          *   (      &      0      3                           $      4   #   2   %   /                  1   
   !                                        +   '             "   ,   .           deleteComment errorAtLeastOneNews errorAtLeastOneNewsCat errorAtLeastOneNewsComment errorNewsCatFirst errorNewsCatNotFound errorNewsImageDelete goOnEdit mainCategory newAdd newEdit news newsActivate newsAlreadyExists newsArchive newsArchiveLast newsAuthor newsCatCreate newsCatEdit newsCatLastUpdate newsCatOverview newsCatParent newsCatSort newsCommentActivate newsCommentEdit newsComments newsDate newsDeleteCat newsDeleteComment newsDeleteNews newsDesc newsInfinite newsMandatoryFields newsMetaDescription newsMetaKeywords newsMetaTitle newsOverview newsPicAdd newsPics newsPraefix newsPreviewText newsSeo newsURL newsValidation successNewsCatDelete successNewsCatSave successNewsCommentDelete successNewsCommentUnlock successNewsCommmentEdit successNewsDelete successNewsImageDelete successNewsSave Content-Type: text/plain; charset=UTF-8
 Möchten Sie die Kommentare wirklich löschen? Bitte wählen Sie mindestens einen Blogbeitrag. Bitte markieren Sie mindestens eine Blogkategorie. Bitte markieren Sie mindestens einen Blogkommentar. Bitte legen Sie zuerst eine Blogkategorie an. Blogkategorie mit ID %d konnte nicht gefunden werden. Ausgewähltes Blogbild konnte nicht gelöscht werden.  und weiter bearbeiten Hauptkategorie Neuen Beitrag erstellen Beitrag bearbeiten Blog Freischalten schon vorhanden Archivieren Archivierte Beiträge Autor Neue Kategorie erstellen Kategorie bearbeiten Letzte Aktualisierung Kategorien Oberkategorie Kategoriesortierung Nicht freigeschaltete Kommentare Kommentar bearbeiten Kommentare Erstellt am Folgende Blogkategorien und deren Unterkategorien werden gelöscht: Kommentar löschen Blogbeitrag löschen Verwenden Sie den Blog von JTL-Shop, um Ihre Kunden auf dem Laufenden zu halten. Erstellen Sie hier neue Blogbeiträge und legen Sie fest, wie die Beiträge dargestellt werden. immer (*) Pflichtfelder Meta Description Meta Keywords Meta Title Beiträge Weiteres Bild hinzufügen Verfügbare Bilder Monatsübersicht-Präfix Vorschautext SEO https://jtl-url.de/2ek6o Erstellt am Markierte Blogkategorien wurden erfolgreich gelöscht. Blogkategorie wurde erfolgreich eingetragen. Markierte Kommentare wurden erfolgreich gelöscht. Markierte Blogkommentare wurden erfolgreich freigeschaltet. Blogkommentar wurde erfolgreich bearbeitet. Markierte Blogbeiträge wurden erfolgreich gelöscht. Ausgewähltes Blogbild wurde erfolgreich gelöscht. Blogbeitrag wurde erfolgreich gespeichert. 