HOOK_QUICKSYNC_XML_BEARBEITEINSERT (225)
========================================

Triggerpunkt
""""""""""""

Nach dem Einfügen eines Artikels in die Datenbank, in ``bearbeiteInsert()`` (in dbeS)

Parameter
"""""""""

``JTL\Catalog\Product\Artikel`` **oArtikel**
    **oArtikel** Artikelobjekt
